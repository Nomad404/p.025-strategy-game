using UnityEngine;

namespace StrategyGame.Grid.Pathfinding
{
    public class PathNode
    {
        public readonly Vector2Int GridPosition;

        /// <summary>
        /// Cost to get from start node to this node
        /// </summary>
        public int GCost;

        /// <summary>
        /// Cost to get from the end node to this node (estimate)
        /// </summary>
        public int HCost;

        /// <summary>
        /// Combined cost for comparison
        /// </summary>
        public int FCost => GCost + HCost;

        public readonly bool IsObstacle;
        public PathNode PreviousNode;

        public PathNode(Vector2Int gridPosition, bool isObstacle = false)
        {
            GridPosition = gridPosition;
            IsObstacle = isObstacle;
        }
    }
}