using System.Collections.Generic;
using System.Linq;
using StrategyGame.Interfaces;
using UnityEngine;

namespace StrategyGame.Grid.Pathfinding
{
    public class Pathfinder : IGridHolder<PathNode>
    {
        private const int MoveCostStraight = 10;
        private const int MoveCostDiag = 14;

        // Complementary grid consisting of pathnodes
        public Grid<PathNode> Grid { get; private set; }

        public void GenerateGrid(Vector2Int gridSize, ICollection<Vector2Int> obstacles)
        {
            Grid = new Grid<PathNode>(gridSize, gridPos => new PathNode(gridPos, obstacles.Contains(gridPos)));
        }

        /// <summary>
        /// Finds path on the grid from one point to another
        /// </summary>
        /// <param name="from">Start position</param>
        /// <param name="to">End position</param>
        /// <param name="maxCost">Maximum cost the path is allowed to take, is ignored if 0</param>
        /// <param name="customObstacles">List of obstacles to include apart from preset ones, used for dynamic obstacles</param>
        /// <param name="ignoreObstacles">Setting to ignore any obstacle</param>
        /// <returns>List of tuples with a path (list of nodes) and the corresponding cost for the path</returns>
        public (IEnumerable<PathNode> path, int cost) FindPath(Vector2Int from, Vector2Int to, int maxCost = 0,
            List<Vector2Int> customObstacles = null, bool ignoreObstacles = false)
        {
            if (!Grid.IsPositionValid(from) || !Grid.IsPositionValid(to)) return (new List<PathNode>(), 0);

            var startNode = Grid.GetObjectAt(from);
            var endNode = Grid.GetObjectAt(to);

            var nodesToCheck = new List<PathNode> {startNode};
            var visitedNodes = new List<PathNode>();

            for (var x = 0; x < Grid.Tiles.GetLength(0); x++)
            {
                for (var y = 0; y < Grid.Tiles.GetLength(1); y++)
                {
                    var node = Grid.GetObjectAt(new Vector2Int(x, y));
                    node.GCost = int.MaxValue;
                    node.PreviousNode = null;
                }
            }

            startNode.GCost = 0;
            startNode.HCost = CalculateDistanceCost(startNode, endNode);

            while (nodesToCheck.Count > 0)
            {
                var currentNode = nodesToCheck.OrderBy(node => node.FCost).First();
                // End condition, construct path
                if (currentNode == endNode)
                {
                    var path = BacktrackPathFrom(endNode);
                    if (!path.Any()) return (new List<PathNode>(), 0);

                    // Only take nodes until max cost is hit
                    if (maxCost > 0)
                    {
                        // Trim path to all nodes that dont exceed the max cost to move
                        for (var i = 0; i < path.Count; i++)
                        {
                            var node = path[i];
                            if (node.GCost <= maxCost) continue;
                            path = path.Take(i).ToList();
                            break;
                        }
                    }

                    return (path, path.Last().GCost);
                }

                nodesToCheck.Remove(currentNode);
                visitedNodes.Add(currentNode);

                // Check all neighbor nodes for the next step
                foreach (var neighborNode in GetNeighborsOf(currentNode)
                             .Where(neighborNode => !visitedNodes.Contains(neighborNode)))
                {
                    // Ignore node if it's an obstacle
                    if (!ignoreObstacles)
                    {
                        if (neighborNode.IsObstacle || (customObstacles != null &&
                                                        customObstacles.Contains(neighborNode.GridPosition)))
                        {
                            visitedNodes.Add(neighborNode);
                            continue;
                        }
                    }

                    // Check if neighbor is the faster option for the path
                    var tempGCost = currentNode.GCost + CalculateDistanceCost(currentNode, neighborNode);
                    if (tempGCost >= neighborNode.GCost) continue;
                    neighborNode.PreviousNode = currentNode;
                    neighborNode.GCost = tempGCost;
                    neighborNode.HCost = CalculateDistanceCost(neighborNode, endNode);

                    if (!nodesToCheck.Contains(neighborNode))
                    {
                        nodesToCheck.Add(neighborNode);
                    }
                }
            }

            // Finished open list
            return (new List<PathNode>(), 0);
        }

        private static List<PathNode> BacktrackPathFrom(PathNode endNode)
        {
            var path = new List<PathNode> {endNode};
            var currentNode = endNode;
            while (currentNode.PreviousNode != null)
            {
                path.Add(currentNode.PreviousNode);
                currentNode = currentNode.PreviousNode;
            }

            path.Reverse();
            return path;
        }

        /// <summary>
        /// Calculates an approximate cost from one node to another
        /// </summary>
        private static int CalculateDistanceCost(PathNode a, PathNode b)
        {
            var distanceX = Mathf.Abs(a.GridPosition.x - b.GridPosition.x);
            var distanceY = Mathf.Abs(a.GridPosition.y - b.GridPosition.y);
            var distanceRemaining = Mathf.Abs(distanceX - distanceY);
            return MoveCostDiag * Mathf.Min(distanceX, distanceY) + MoveCostStraight * distanceRemaining;
        }

        /// <summary>
        /// Returns all neighbors of a node
        /// </summary>
        private IEnumerable<PathNode> GetNeighborsOf(PathNode node)
        {
            var neighborList = new List<PathNode>();
            var nodePos = node.GridPosition;
            if (nodePos.x > 0)
            {
                // Left
                neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x - 1, nodePos.y)));

                if (nodePos.y > 0)
                {
                    // Down
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x, nodePos.y - 1)));
                    // Down Left
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x - 1, nodePos.y - 1)));
                }

                if (nodePos.y < Grid.GridSize.y - 1)
                {
                    // Up
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x, nodePos.y + 1)));
                    // Up Left
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x - 1, nodePos.y + 1)));
                }
            }

            if (node.GridPosition.x < Grid.GridSize.x - 1)
            {
                // Right
                neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x + 1, nodePos.y)));
                // Down Right
                if (nodePos.y > 0)
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x + 1, nodePos.y - 1)));
                // Up Right
                if (nodePos.y < Grid.GridSize.y - 1)
                    neighborList.Add(Grid.GetObjectAt(new Vector2Int(nodePos.x + 1, nodePos.y + 1)));
            }

            return neighborList;
        }
    }
}