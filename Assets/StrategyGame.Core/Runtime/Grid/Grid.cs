using System;
using UnityEngine;

namespace StrategyGame.Grid
{
    /// <summary>
    /// Abstract representation of a grid
    /// </summary>
    /// <typeparam name="TGridObject"></typeparam>
    public class Grid<TGridObject>
    {
        public readonly Vector2Int GridSize;

        public readonly TGridObject[,] Tiles;

        public Grid(Vector2Int gridSize, Func<Vector2Int, TGridObject> createGridCell)
        {
            GridSize = gridSize;
            Tiles = new TGridObject[GridSize.x, GridSize.y];
            for (var x = 0; x < Tiles.GetLength(0); x++)
            {
                for (var y = 0; y < Tiles.GetLength(1); y++)
                {
                    Tiles[x, y] = createGridCell.Invoke(new Vector2Int(x, y));
                }
            }
        }

        public TGridObject GetObjectAt(Vector2Int pos)
        {
            if (!IsPositionValid(pos)) throw new ArgumentException("Position has to be on the grid");
            return Tiles[pos.x, pos.y];
        }

        public bool IsPositionValid(Vector2Int position)
        {
            return position.x >= 0 && position.y >= 0 && position.x < GridSize.x && position.y < GridSize.y;
        }
    }
}