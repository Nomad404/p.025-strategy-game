using System;
using System.Linq;
using StrategyGame.Controllers;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEditor;
using UnityEngine;

namespace StrategyGame.Grid
{
    /// <summary>
    /// Visual representation of a grid tile
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class GridTileDisplay : MonoBehaviour
    {
        [SerializeField] private Color tileColorEven;
        [SerializeField] private Color tileColorUneven;
        [SerializeField] private Color tileColorHighlighted;
        [SerializeField] private Color tileColorHover;
        [SerializeField] private Color tileColorSelected;
        [SerializeField] private Color tileColorAttack;
        [SerializeField] private Color tileColorAttackHover;
        [SerializeField] private Color tileColorObstacle;

        public GridTile Tile { get; set; }
        public Vector3 DisplaySize => GetComponent<Renderer>().bounds.size;

        private TileState _tileState;

        private TileState State
        {
            get => _tileState;
            set
            {
                if (_tileState == value) return;
                _tileState = value;
                UpdateTileColor();
            }
        }

        private bool _isEven;

        public bool IsEven
        {
            set
            {
                _isEven = value;
                UpdateTileColor();
            }
        }

        private Renderer Renderer => GetComponent<Renderer>();

        public IGridVisualizer<GridTile, GridTileDisplay> GridVisualizer { get; set; }

        private void Update()
        {
            if (Tile.IsObstacle)
            {
                State = TileState.Obstacle;
                return;
            }

            if (IsInCurrentSelection() || IsInQueue())
            {
                return;
            }

            State = TileState.Default;
        }

        /// <summary>
        /// Checks if tile is in the current selection of tiles and sets the state accordingly.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private bool IsInCurrentSelection()
        {
            var currentSelection = Tile.GridHandler.CurrentTileSelection;
            if (currentSelection is not {Count: > 0}) return false;
            if (GridVisualizer == null) return false;
            switch (GridVisualizer.State)
            {
                case GridState.ShowMovePath:
                    if (currentSelection.Last() == Tile.GridPosition)
                    {
                        State = TileState.Hover;
                        return true;
                    }

                    if (currentSelection.Contains(Tile.GridPosition))
                    {
                        State = TileState.Highlighted;
                        return true;
                    }

                    break;
                case GridState.ShowAttackRange:
                    if (Tile.GridHandler.CurrentPointedTile == Tile)
                    {
                        State = TileState.AttackHover;
                        return true;
                    }

                    if (currentSelection.Contains(Tile.GridPosition))
                    {
                        State = TileState.Attack;
                        return true;
                    }

                    break;
            }

            return false;
        }

        /// <summary>
        /// Checks if tile is in any queue of any unit and sets the state based on that action
        /// </summary>
        /// <returns></returns>
        private bool IsInQueue()
        {
            var unit = UnitController.Instance.CurrentSelectedUnit;
            if (unit == null) return false;
            var actions = unit.ActionsQueue.ToList();
            if (actions.Count <= 0) return false;
            foreach (var action in actions)
            {
                switch (action)
                {
                    case UnitActionMove moveAction:
                    {
                        var path = moveAction.Path;
                        if (path.Last() == Tile.GridPosition)
                        {
                            State = TileState.Hover;
                            return true;
                        }

                        if (path.Contains(Tile.GridPosition))
                        {
                            State = TileState.Highlighted;
                            return true;
                        }

                        break;
                    }
                    case UnitActionAttack attackAction:
                    {
                        if (attackAction.TargetTile == Tile)
                        {
                            State = TileState.AttackHover;
                            return true;
                        }

                        var area = attackAction.Area;
                        if (area.Contains(Tile.GridPosition))
                        {
                            State = TileState.Attack;
                            return true;
                        }

                        break;
                    }
                }
            }

            return false;
        }

        private void OnMouseEnter()
        {
            if (MainLevelController.Instance.LevelState != LevelState.PlayerTurn) return;
            GridVisualizer.CurrentPointedTile = Tile;
        }

        private void OnMouseDown()
        {
            if (MainLevelController.Instance.LevelState != LevelState.PlayerTurn) return;
            Tile.OnTileSelected();
        }

        private void UpdateTileColor()
        {
            // Necessary for running within the editor to prevent material leak
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying)
            {
                Renderer.sharedMaterial.color = GetTileColorByState(State);
            }
            else
            {
                Renderer.material.color = GetTileColorByState(State);
            }
#else
            Renderer.material.color = GetTileColorByState(State);
#endif
        }

        private Color GetTileColorByState(TileState state)
        {
            return state switch
            {
                TileState.Default => _isEven ? tileColorEven : tileColorUneven,
                TileState.Highlighted => tileColorHighlighted,
                TileState.Hover => tileColorHover,
                TileState.Selected => tileColorSelected,
                TileState.Attack => tileColorAttack,
                TileState.AttackHover => tileColorAttackHover,
                TileState.Obstacle => tileColorObstacle,
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }

        private enum TileState
        {
            Default,
            Highlighted,
            Hover,
            Selected,
            Attack,
            AttackHover,
            Obstacle,
        }
    }
}