using System;
using System.Linq;
using StrategyGame.Controllers;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEngine;

namespace StrategyGame.Grid
{
    public class GridTile
    {
        private static MainLevelController MainLevelController => MainLevelController.Instance;
        private static UnitController UnitController => UnitController.Instance;

        public readonly Vector2Int GridPosition;
        public readonly Vector3 WorldPosition;

        public bool IsObstacle { get; }
        public UnitDisplay UnitInPlace;

        public readonly IGridHandler<GridTile> GridHandler;

        public GridTile(Vector2Int gridPosition, Vector3 worldPosition, IGridHandler<GridTile> gridHandler,
            bool isObstacle = false)
        {
            GridPosition = gridPosition;
            WorldPosition = worldPosition;
            IsObstacle = isObstacle;
            GridHandler = gridHandler;
        }

        public bool IsWalkable()
        {
            // Get all actions currently in queue
            var actions = MainLevelController.UnitQueue.ToList().SelectMany(unit => unit.ActionsQueue.ToList())
                .ToList();
            // Check if any action has already reserved the tile
            var tilesForActions = actions.Select(action =>
            {
                return action switch
                {
                    UnitActionMove actionMove => actionMove.Path.Last(),
                    UnitActionAttack actionAttack => actionAttack.TargetTile.GridPosition,
                    _ => throw new ArgumentOutOfRangeException()
                };
            });
            return UnitInPlace == null && !tilesForActions.Contains(GridPosition);
        }

        public void OnTileSelected()
        {
            var unit = UnitController.CurrentSelectedUnit;
            if (unit == null) return;
            switch (GridHandler.State)
            {
                case GridState.Default:
                    break;
                case GridState.ShowMovePath:
                    UnitController.AddMoveActionToUnit(unit, GridHandler.CurrentTileSelection);
                    break;
                case GridState.ShowAttackRange:
                    UnitController.AddAttackActionToUnit(unit, this, GridHandler.CurrentTileSelection,
                        GridController.Instance.CurrentPathCost);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}