using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StrategyGame.Controllers;
using StrategyGame.Interfaces;
using UnityEngine;

namespace StrategyGame.Units
{
    /// <summary>
    /// Visual representation of a unit
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public class UnitDisplay : MonoBehaviour
    {
        private static MainLevelController MainLevelController => MainLevelController.Instance;
        private static GridController GridController => GridController.Instance;
        private static UnitController UnitController => UnitController.Instance;

        private static readonly int FirstOutlineWidth = Shader.PropertyToID("_FirstOutlineWidth");
        private const float MoveOverTimeThreshold = 0.001f;

        private bool _isSelected;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                if (Faction != UnitFaction.Hostile)
                    _renderer.material.SetFloat(FirstOutlineWidth, _isSelected ? 0.05f : 0f);
            }
        }

        public Vector2Int CurrentGridPosition { get; set; }

        private Renderer _renderer;

        public UnitFaction Faction { get; set; }

        public readonly Queue<UnitAction> ActionsQueue = new();

        public Vector2Int LatestActionPoint
        {
            get
            {
                if (ActionsQueue.Count == 0) return CurrentGridPosition;
                var action = ActionsQueue.Last();
                return action switch
                {
                    UnitActionMove => action.TargetPoint,
                    UnitActionAttack => action.SourcePoint,
                    _ => CurrentGridPosition
                };
            }
        }

        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
        }

        private void OnMouseDown()
        {
            if (MainLevelController.LevelState == LevelState.EnemyTurn && Faction == UnitFaction.Hostile) return;

            // Prevent unit selection while moving or attacking, excluding when player is selecting an attack target
            if (GridController.State != GridState.ShowAttackRange)
            {
                switch (Faction)
                {
                    case UnitFaction.Friendly when MainLevelController.LevelState != LevelState.PlayerTurn:
                    case UnitFaction.Hostile when MainLevelController.LevelState != LevelState.EnemyTurn:
                        return;
                }
            }

            // Attack this unit if current selected unit is in attack mode
            var currentUnit = UnitController.CurrentSelectedUnit;
            if (currentUnit != null)
            {
                var tile = GridController.Grid.Tiles[CurrentGridPosition.x, CurrentGridPosition.y];
                if (GridController.State == GridState.ShowAttackRange)
                {
                    if (currentUnit.Faction != Faction)
                    {
                        UnitController.AddAttackActionToUnit(currentUnit, tile, GridController.CurrentTileSelection,
                            GridController.CurrentPathCost);
                        return;
                    }

                    UIController.Instance.AddLogMessage("You can't attack friendly units", Color.red);
                    return;
                }
            }

            UnitController.SelectUnit(UnitController.CurrentSelectedUnit == this ? null : this);
        }

        public void AddActionToQueue(UnitAction action)
        {
            ActionsQueue.Enqueue(action);
        }

        public IEnumerator Seq_ExecuteQueue()
        {
            while (ActionsQueue.Count > 0)
            {
                yield return ActionsQueue.Dequeue().Execute();
                yield return new WaitForSeconds(0.5f);
            }
        }

        public void TurnTowards(Vector3 pointTowards)
        {
            var targetDirection = pointTowards - transform.position;
            var newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 10f, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }

        public void StartAttack()
        {
            Shake(0.2f, 0.1f, includeYAxis: false);
        }

        public void TakeDamage()
        {
            Shake(0.5f, 0.3f, includeYAxis: false);
            Destroy(gameObject, 0.5f);
            switch (Faction)
            {
                case UnitFaction.Friendly:
                    UnitController.Allies.Remove(this);
                    break;
                case UnitFaction.Hostile:
                    UnitController.Enemies.Remove(this);
                    break;
            }
        }

        public IEnumerator Seq_MoveOnPathOverTime(IEnumerable<Vector3> path, float durationPerMove)
        {
            return path.Select(destination => Seq_MoveToOverTime(destination, durationPerMove)).GetEnumerator();
        }

        private IEnumerator Seq_MoveToOverTime(Vector3 destination, float duration)
        {
            TurnTowards(destination);
            var startPos = transform.position;
            var t = 0f;
            while (Vector3.Distance(transform.position, destination) > MoveOverTimeThreshold)
            {
                t += Time.fixedDeltaTime / duration;
                transform.position = Vector3.Lerp(startPos, destination, t);
                yield return new WaitForFixedUpdate();
            }
        }

        private void Shake(float duration, float magnitude, bool includeXAxis = true,
            bool includeYAxis = true, bool includeZAxis = true)
        {
            StartCoroutine(Seq_Shake(duration, magnitude, includeXAxis, includeYAxis, includeZAxis));
        }

        private IEnumerator Seq_Shake(float duration, float magnitude, bool includeXAxis = true,
            bool includeYAxis = true, bool includeZAxis = true)
        {
            var originalPosition = transform.position;
            var elapsed = 0f;

            while (elapsed < duration)
            {
                var newX = includeXAxis ? originalPosition.x + Random.Range(-1f, 1f) * magnitude : originalPosition.x;
                var newY = includeYAxis ? originalPosition.y + Random.Range(-1, 1f) * magnitude : originalPosition.y;
                var newZ = includeZAxis ? originalPosition.z + Random.Range(-1f, 1f) * magnitude : originalPosition.z;
                transform.position = new Vector3(newX, newY, newZ);
                elapsed += Time.deltaTime;
                yield return null;
            }

            transform.position = originalPosition;
        }

        public enum UnitFaction
        {
            Neutral = 0,
            Friendly = 1,
            Hostile = 2
        }
    }
}