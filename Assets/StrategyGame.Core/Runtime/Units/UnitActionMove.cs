using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StrategyGame.Grid;
using UnityEngine;
using ILogger = StrategyGame.Interfaces.ILogger;

namespace StrategyGame.Units
{
    /// <summary>
    /// Move action to be enqueued for a unit
    /// </summary>
    public class UnitActionMove : UnitAction
    {
        private const float MoveDurationPerStep = 0.1f;

        public readonly UnitDisplay Unit;

        public List<Vector2Int> Path => Tiles;

        private readonly GridTile[,] _grid;
        private readonly ILogger _logger;

        public UnitActionMove(UnitDisplay unit, List<Vector2Int> path, int cost, GridTile[,] grid,
            ILogger logger) : base(
            path.First(), path.Last(), path,
            cost)
        {
            Unit = unit;
            _grid = grid;
            _logger = logger;
        }

        public override IEnumerator Execute()
        {
            if (Path.Count == 0) yield break;
            _logger.AddLogMessage($"Moving {Unit.name} to {Path.Last()}");
            // Convert into world positions
            var worldPath = Path.Select(pos =>
            {
                var worldPos = _grid[pos.x, pos.y].WorldPosition;
                return new Vector3(worldPos.x, Unit.transform.position.y, worldPos.z);
            }).ToList();
            yield return Unit.Seq_MoveOnPathOverTime(worldPath, MoveDurationPerStep);
            // Clear previous tile of occupant
            var firstPos = Path.First();
            _grid[firstPos.x, firstPos.y].UnitInPlace = null;
            // Set unit at new tile
            var lastPos = Path.Last();
            Unit.CurrentGridPosition = lastPos;
            _grid[lastPos.x, lastPos.y].UnitInPlace = Unit;
        }
    }
}