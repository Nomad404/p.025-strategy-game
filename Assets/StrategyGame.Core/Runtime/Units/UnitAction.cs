using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StrategyGame.Units
{
    /// <summary>
    /// Parent class of all unit actions
    /// </summary>
    public abstract class UnitAction
    {
        public readonly Vector2Int SourcePoint;
        public readonly Vector2Int TargetPoint;
        public readonly List<Vector2Int> Tiles;
        public readonly int Cost;

        protected UnitAction(Vector2Int sourcePoint, Vector2Int targetPoint, List<Vector2Int> tiles, int cost)
        {
            SourcePoint = sourcePoint;
            TargetPoint = targetPoint;
            Tiles = tiles;
            Cost = cost;
        }

        public abstract IEnumerator Execute();
    }
}