using System.Collections;
using System.Collections.Generic;
using StrategyGame.Grid;
using UnityEngine;
using ILogger = StrategyGame.Interfaces.ILogger;

namespace StrategyGame.Units
{
    /// <summary>
    /// Attack action to be enqueued for a unit
    /// </summary>
    public class UnitActionAttack : UnitAction
    {
        public readonly UnitDisplay SourceUnit;
        public readonly UnitDisplay TargetUnit;
        public readonly GridTile TargetTile;
        public List<Vector2Int> Area => Tiles;

        private readonly ILogger _logger;

        public UnitActionAttack(UnitDisplay sourceUnit, GridTile targetTile, Vector2Int center, List<Vector2Int> area,
            int cost, ILogger logger) : base(
            center, targetTile.GridPosition, area, cost)
        {
            SourceUnit = sourceUnit;
            TargetTile = targetTile;
            TargetUnit = TargetTile.UnitInPlace;
            _logger = logger;
        }

        public override IEnumerator Execute()
        {
            if (SourceUnit == null) yield break;
            if (TargetUnit == null)
            {
                _logger.AddLogMessage("Select a unit to attack");
                yield break;
            }

            _logger.AddLogMessage($"{SourceUnit.name} attacks {TargetUnit.name}");

            SourceUnit.TurnTowards(TargetUnit.transform.position);
            SourceUnit.StartAttack();
            TargetUnit.TakeDamage();
        }
    }
}