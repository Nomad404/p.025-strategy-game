using StrategyGame.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StrategyGame.UI
{
    [RequireComponent(typeof(Button))]
    public class UISubmitButton : MonoBehaviour
    {
        [SerializeField] private string buttonText;
        [SerializeField] private KeyCode submitKey = KeyCode.Return;

        private Button _button;
        private TextMeshProUGUI _buttonText;

        private void OnValidate()
        {
            SetupComponents();
            _buttonText.text = $"[{submitKey.ToString()}] {buttonText}";
        }

        private void Awake()
        {
            SetupComponents();
            _button.interactable = false;
            _buttonText.text = $"[{submitKey.ToString()}] {buttonText}";
        }

        private void SetupComponents()
        {
            _button = GetComponent<Button>();
            _buttonText = GetComponentInChildren<TextMeshProUGUI>();
        }

        private void Update()
        {
            var unitQueueEmpty = MainLevelController.Instance.UnitQueue.Count == 0;
            var isButtonEnabled = _button.interactable;

            _button.interactable = isButtonEnabled switch
            {
                true when unitQueueEmpty => false,
                false when !unitQueueEmpty => true,
                _ => isButtonEnabled
            };

            if (Input.GetKeyDown(submitKey))
            {
                SubmitActions();
            }
        }

        public void SubmitActions()
        {
            MainLevelController.Instance.ExecuteQueue();
        }
    }
}