using StrategyGame.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StrategyGame.UI
{
    [RequireComponent(typeof(Button))]
    public class UIClearQueueButton : MonoBehaviour
    {
        [SerializeField] private string buttonText;
        [SerializeField] private KeyCode clearQueueKey = KeyCode.Q;

        private Button _button;
        private TextMeshProUGUI _buttonText;

        private void OnValidate()
        {
            SetupComponents();
            _buttonText.text = $"[{clearQueueKey}] {buttonText}";
        }

        private void Awake()
        {
            SetupComponents();
            _button.interactable = false;
            _buttonText.text = $"[{clearQueueKey}] {buttonText}";
        }

        private void SetupComponents()
        {
            _button = GetComponent<Button>();
            _buttonText = GetComponentInChildren<TextMeshProUGUI>();
        }

        private void Update()
        {
            var currentUnit = UnitController.Instance.CurrentSelectedUnit;
            var canClearQueue = currentUnit != null && currentUnit.ActionsQueue.Count > 0;
            _button.interactable = canClearQueue;

            if (Input.GetKeyDown(clearQueueKey) && canClearQueue)
            {
                currentUnit.ActionsQueue.Clear();
                MainLevelController.Instance.RemoveUnitFromQueue(currentUnit);
            }
        }

        public void ClearActionsQueue()
        {
            var currentUnit = UnitController.Instance.CurrentSelectedUnit;
            if (currentUnit == null || currentUnit.ActionsQueue.Count == 0) return;
            currentUnit.ActionsQueue.Clear();
            MainLevelController.Instance.RemoveUnitFromQueue(currentUnit);
        }
    }
}