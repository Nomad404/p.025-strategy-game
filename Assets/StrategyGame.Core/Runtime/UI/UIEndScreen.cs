using TMPro;
using UnityEngine;

namespace StrategyGame.UI
{
    public class UIEndScreen : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI headerText;

        public void SetHeaderText(string text)
        {
            headerText.text = text;
        }
    }
}