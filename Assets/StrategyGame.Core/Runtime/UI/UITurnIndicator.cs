using System.Collections;
using StrategyGame.Controllers;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace StrategyGame.UI
{
    public class UITurnIndicator : MonoBehaviour
    {
        private const float ScaleOverTimeThreshold = 0.001f;
        private const float ScaleSpeed = 0.3f;

        [SerializeField] private Color indicatorColorAllies;
        [SerializeField] private Color indicatorColorEnemies;
        [SerializeField] private Color indicatorColorNeutral;

        [SerializeField] private Image indicatorArrow;
        [SerializeField] private UICircle indicatorBackground;

        private IEnumerator _arrowScaleEnumerator;

        private bool IsLeft => indicatorArrow.transform.localScale.x > 0;

        private void Awake()
        {
            indicatorBackground.color =
                indicatorArrow.transform.localScale.x > 0 ? indicatorColorAllies : indicatorColorEnemies;
        }

        private void Start()
        {
            MainLevelController.Instance.OnNewLevelState += state =>
            {
                switch (state)
                {
                    case LevelState.PlayerTurn:
                        if (!IsLeft) SetLeft();
                        break;
                    case LevelState.EnemyTurn:
                        if (IsLeft) SetRight();
                        break;
                    default:
                        SetNeutral();
                        break;
                }
            };
        }

        private void SetRight()
        {
            indicatorBackground.color = indicatorColorEnemies;
            UIController.Instance.AddLogMessage($"Enemy turn", Color.red);
            StopScaleLerpIfRunning();
            _arrowScaleEnumerator = Seq_ScaleOverTime(indicatorArrow.transform, new Vector3(-1, 1, 1), ScaleSpeed);
            StartCoroutine(_arrowScaleEnumerator);
        }

        private void SetLeft()
        {
            indicatorBackground.color = indicatorColorAllies;
            UIController.Instance.AddLogMessage($"Allies turn", Color.blue);
            StopScaleLerpIfRunning();
            _arrowScaleEnumerator = Seq_ScaleOverTime(indicatorArrow.transform, new Vector3(1, 1, 1), ScaleSpeed);
            StartCoroutine(_arrowScaleEnumerator);
        }

        private void SetNeutral()
        {
            indicatorBackground.color = indicatorColorNeutral;
            StopScaleLerpIfRunning();
            _arrowScaleEnumerator = Seq_ScaleOverTime(indicatorArrow.transform, new Vector3(0, 1, 1), ScaleSpeed);
            StartCoroutine(_arrowScaleEnumerator);
        }

        private void StopScaleLerpIfRunning()
        {
            if (_arrowScaleEnumerator != null)
            {
                StopCoroutine(_arrowScaleEnumerator);
            }
        }

        private static IEnumerator Seq_ScaleOverTime(Transform targetTransform, Vector3 targetScale, float duration)
        {
            var startScale = targetTransform.localScale;
            var t = 0f;
            while (Vector3.Distance(targetTransform.localScale, targetScale) > ScaleOverTimeThreshold)
            {
                t += Time.fixedDeltaTime / duration;
                targetTransform.localScale = Vector3.Lerp(startScale, targetScale, t);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}