using TMPro;
using UnityEngine;

namespace StrategyGame.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class UILogMessage : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
        }

        public void SetText(string text)
        {
            _text.text = text;
        }

        public void SetTextColor(Color color)
        {
            _text.color = color;
        }
    }
}