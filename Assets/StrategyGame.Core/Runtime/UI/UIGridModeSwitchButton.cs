using System;
using StrategyGame.Controllers;
using StrategyGame.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StrategyGame.UI
{
    [RequireComponent(typeof(Button))]
    public class UIGridModeSwitchButton : MonoBehaviour
    {
        [SerializeField] private GridState defaultState;
        [SerializeField] private ButtonSetting buttonSettingMove;
        [SerializeField] private ButtonSetting buttonSettingAttack;

        [Header("Controls"), SerializeField] private KeyCode switchKey = KeyCode.S;

        public Button button;
        private TextMeshProUGUI _buttonText;

        private void OnValidate()
        {
            SetupComponents();
            UpdateByGridState(defaultState);
        }

        private void Awake()
        {
            SetupComponents();
            UpdateByGridState(GridState.ShowMovePath);
        }

        private void SetupComponents()
        {
            button = GetComponent<Button>();
            _buttonText = GetComponentInChildren<TextMeshProUGUI>();
        }

        private void Start()
        {
            if (UnitController.Instance.CurrentSelectedUnit == null) button.interactable = false;
        }

        private void Update()
        {
            if (Input.GetKeyDown(switchKey))
            {
                SwitchMode();
            }
        }

        public void SwitchMode()
        {
            var grid = GridController.Instance;

            grid.State = grid.State switch
            {
                GridState.ShowMovePath => GridState.ShowAttackRange,
                GridState.ShowAttackRange => GridState.ShowMovePath,
                _ => grid.State
            };

            UpdateByGridState(grid.State);
        }

        public void UpdateByGridState(GridState state)
        {
            switch (state)
            {
                case GridState.ShowMovePath:
                    SetButtonSetting(buttonSettingAttack);
                    break;
                case GridState.ShowAttackRange:
                    SetButtonSetting(buttonSettingMove);
                    break;
                case GridState.Default:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetButtonSetting(ButtonSetting setting)
        {
            _buttonText.text = $"[{switchKey}] {setting.text}";
            _buttonText.color = setting.textColor;
            button.colors = setting.buttonColors;
        }

        [Serializable]
        public struct ButtonSetting
        {
            public string text;
            public Color textColor;
            public ColorBlock buttonColors;

            public ButtonSetting(string text, Color textColor, ColorBlock buttonColors)
            {
                this.text = text;
                this.textColor = textColor;
                this.buttonColors = buttonColors;
            }
        }
    }
}