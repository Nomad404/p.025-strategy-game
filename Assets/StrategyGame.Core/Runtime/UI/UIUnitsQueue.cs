using System.Linq;
using StrategyGame.Controllers;
using TMPro;
using UnityEngine;

namespace StrategyGame.UI
{
    public class UIUnitsQueue : MonoBehaviour
    {
        private static MainLevelController MainLevelController => MainLevelController.Instance;

        [SerializeField] private TextMeshProUGUI text;

        private void Awake()
        {
            MainLevelController.OnUnitQueueChanged += () =>
            {
                var entries = MainLevelController.UnitQueue
                    .Select(unit => $"{unit.name}").ToArray();
                text.text = string.Join("\n", entries);
            };
        }
    }
}