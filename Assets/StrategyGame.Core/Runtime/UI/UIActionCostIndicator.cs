using StrategyGame.Controllers;
using TMPro;
using UnityEngine;

namespace StrategyGame.UI
{
    public class UIActionCostIndicator : MonoBehaviour
    {
        private static MainLevelController MainLevelController => MainLevelController.Instance;

        [Header("Objects"), SerializeField] private RectTransform indicatorFill;
        [SerializeField] private TextMeshProUGUI textObject;

        private void OnValidate()
        {
            UpdateByCostValue();
        }

        private void Update()
        {
            UpdateByCostValue(MainLevelController.CurrentActionCostSum);
        }

        private void UpdateByCostValue(int currentCostSum = 0)
        {
            var ratio = (float) currentCostSum / GridController.MaxActionCost;
            indicatorFill.localScale = new Vector3(ratio, 1, 1);
            textObject.text = $"{currentCostSum}/{GridController.MaxActionCost}";
        }
    }
}