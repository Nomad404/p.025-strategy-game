using StrategyGame.Controllers;
using UnityEngine;

namespace StrategyGame.UI
{
    public class UIMainMenuController : MonoBehaviour
    {
        public void StartGame()
        {
            GameController.Instance.LoadMainLevel();
        }

        public void QuitGame()
        {
            GameController.Instance.QuitGame();
        }
    }
}