using StrategyGame.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StrategyGame.UI
{
    public class UIPauseMenu : MonoBehaviour
    {
        [SerializeField] private GameObject contentContainer;

        [Header("Pause Button"), SerializeField]
        private Button pauseButton;

        [SerializeField] private string pauseButtonText;
        [SerializeField] private KeyCode pauseKey = KeyCode.Escape;

        private TextMeshProUGUI _pauseButtonText;

        private void OnValidate()
        {
            UpdatePauseButton();
        }

        private void Awake()
        {
            UpdatePauseButton();
        }

        private void Update()
        {
            if (Input.GetKeyDown(pauseKey))
            {
                TogglePauseMenu();
            }
        }

        private void UpdatePauseButton()
        {
            _pauseButtonText = pauseButton.GetComponentInChildren<TextMeshProUGUI>();
            _pauseButtonText.text = $"[{pauseKey}] {pauseButtonText}";
        }

        private void TogglePauseMenu()
        {
            if (contentContainer.gameObject.activeSelf)
            {
                ClosePauseMenu();
            }
            else
            {
                OpenPauseMenu();
            }
        }

        public void OpenPauseMenu()
        {
            contentContainer.gameObject.SetActive(true);
            GameTime.IsPaused = true;
        }

        public void ClosePauseMenu()
        {
            contentContainer.gameObject.SetActive(false);
            GameTime.IsPaused = false;
        }
    }
}