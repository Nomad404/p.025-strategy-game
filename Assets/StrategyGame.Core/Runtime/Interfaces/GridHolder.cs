using System.Collections.Generic;
using StrategyGame.Grid;
using UnityEngine;

namespace StrategyGame.Interfaces
{
    public interface IGridHolder<TGridObject>
    {
        public Grid<TGridObject> Grid { get; }
    }

    public interface IGridHandler<TGridObject> : IGridHolder<TGridObject>
    {
        public GridState State { get; }
        public TGridObject CurrentPointedTile { get; set; }
        public List<Vector2Int> CurrentTileSelection { get; }
        public TGridObject GetGridTileAt(Vector2Int pos);
    }

    public interface IGridVisualizer<TGridObject, out TGridDisplayObject> : IGridHandler<TGridObject>
    {
        public TGridDisplayObject[,] GridDisplays { get; }
        public IEnumerable<GridTileDisplay> GetAllGridDisplaysInRange(Vector2Int gridPos, float distance);
    }

    public enum GridState
    {
        Default,
        ShowMovePath,
        ShowAttackRange,
    }
}