using UnityEngine;

namespace StrategyGame.Interfaces
{
    public interface ILogger
    {
        public void AddLogMessage(string text);
        public void AddLogMessage(string text, Color color);
    }
}
