using StrategyGame.Controllers;
using UnityEditor;
using UnityEngine;

namespace StrategyGame.Editor
{
    [CustomEditor(typeof(GridController))]
    public class GridControllerEditor : UnityEditor.Editor
    {
        private readonly Color _gridBackgroundColor = new(0.2363469f, 0.3568628f, 0.2078431f, 1f);
        private SerializedProperty _gridSize;
        private SerializedProperty _obstaclePositions;

        private void OnEnable()
        {
            _gridSize = serializedObject.FindProperty("gridSize");
            _obstaclePositions = serializedObject.FindProperty("obstaclePositions");
        }

        public override void OnInspectorGUI()
        {
            var controller = (GridController) target;
            if (DrawDefaultInspector())
            {
                if (controller.autoUpdate)
                {
                    controller.GenerateFullGrid();
                }
            }

            // Draw grid for obstacle editing
            DrawDefaultButtons(controller);
            EditorGUILayout.Space();
            var gridRect = DrawObstaclesGrid();
            CheckInteractionEvents(gridRect);
            DrawObstacleTiles(gridRect);
            EditorGUILayout.Space();

            serializedObject.ApplyModifiedProperties();
        }

        private Rect DrawObstaclesGrid()
        {
            var gSize = _gridSize.vector2IntValue;
            var ratio = (float) gSize.y / gSize.x;
            var gridWidth = EditorGUIUtility.currentViewWidth * 0.9f;
            var gridHeight = gridWidth * ratio;
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(gridWidth), GUILayout.Height(gridHeight));
            GUI.Box(rect, GUIContent.none);
            Handles.DrawSolidRectangleWithOutline(rect, _gridBackgroundColor, Color.white);
            Handles.color = new Color(0f, 0f, 0f, 0.2f);
            DrawGridLines(rect, gSize.x, gSize.y);
            return rect;
        }

        private void CheckInteractionEvents(Rect rect)
        {
            var gSize = _gridSize.vector2IntValue;
            var borderRect = Rect.MinMaxRect(rect.xMin, rect.yMin, rect.xMax, rect.yMax);
            var insideBorderRect = borderRect.Contains(Event.current.mousePosition);

            if (Event.current.type != EventType.MouseDown || Event.current.button != 0 || !insideBorderRect) return;
            // Transform mouse position into index positions on grid
            var currentMousePos = Event.current.mousePosition;
            var rectRelativeMousePos =
                new Vector2(currentMousePos.x - borderRect.xMin, currentMousePos.y - borderRect.yMin);
            var gridRelativePos = new Vector2(rectRelativeMousePos.x,
                borderRect.yMax - rectRelativeMousePos.y - borderRect.yMin);
            var gridCellSize = new Vector2((borderRect.xMax - borderRect.xMin) / gSize.x,
                (borderRect.yMax - borderRect.yMin) / gSize.y);
            var clampedGridPos =
                new Vector2Int((int) (gridRelativePos.x / gridCellSize.x), (int) (gridRelativePos.y / gridCellSize.y));
            CheckIfObstaclePosition(clampedGridPos);
        }

        private void CheckIfObstaclePosition(Vector2Int gridPos)
        {
            if (_obstaclePositions.arraySize > 0)
            {
                for (var i = 0; i < _obstaclePositions.arraySize; i++)
                {
                    var pos = _obstaclePositions.GetArrayElementAtIndex(i).vector2IntValue;
                    if (gridPos != pos) continue;
                    _obstaclePositions.DeleteArrayElementAtIndex(i);
                    return;
                }
            }

            _obstaclePositions.InsertArrayElementAtIndex(Mathf.Clamp(_obstaclePositions.arraySize - 1, 0,
                int.MaxValue));
            _obstaclePositions.GetArrayElementAtIndex(Mathf.Clamp(_obstaclePositions.arraySize - 1, 0, int.MaxValue))
                .vector2IntValue = gridPos;
        }

        private void DrawObstacleTiles(Rect boundsRect)
        {
            if (_obstaclePositions.arraySize == 0) return;
            var gSize = _gridSize.vector2IntValue;
            var gridCellSize = new Vector2((boundsRect.xMax - boundsRect.xMin) / gSize.x,
                (boundsRect.yMax - boundsRect.yMin) / gSize.y);
            for (var i = 0; i < _obstaclePositions.arraySize; i++)
            {
                var pos = _obstaclePositions.GetArrayElementAtIndex(i).vector2IntValue;
                var rect = Rect.MinMaxRect(boundsRect.xMin + gridCellSize.x * pos.x,
                    boundsRect.yMax - gridCellSize.y * (pos.y + 1),
                    boundsRect.xMin + gridCellSize.x * (pos.x + 1),
                    boundsRect.yMax - gridCellSize.y * pos.y);
                EditorGUI.DrawRect(rect, Color.black);
            }
        }

        private static void DrawGridLines(Rect rect, int divisionsX, int divisionsY)
        {
            for (var i = 0; i <= divisionsX; i++)
            {
                var t = Mathf.InverseLerp(0, divisionsX, i);
                var tx = Mathf.Lerp(rect.xMin, rect.xMax, t);
                Handles.DrawLine(new Vector3(tx, rect.yMin), new Vector3(tx, rect.yMax));
            }

            for (var i = 0; i <= divisionsY; i++)
            {
                var t = Mathf.InverseLerp(0, divisionsY, i);
                var ty = Mathf.Lerp(rect.yMin, rect.yMax, t);
                Handles.DrawLine(new Vector3(rect.xMin, ty), new Vector3(rect.xMax, ty));
            }
        }

        private void DrawDefaultButtons(GridController controller)
        {
            if (GUILayout.Button("Clear grid"))
            {
                GridController.ClearGrid();
            }

            if (GUILayout.Button("Generate grid"))
            {
                controller.GenerateFullGrid();
            }

            if (GUILayout.Button("Clear obstacles"))
            {
                _obstaclePositions.ClearArray();
            }
        }
    }
}