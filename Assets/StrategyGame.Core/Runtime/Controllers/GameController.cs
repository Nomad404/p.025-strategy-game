using UnityEngine;
using UnityEngine.SceneManagement;

namespace StrategyGame.Controllers
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void LoadMainLevel()
        {
            SceneManager.LoadScene(LevelNames.MainLevel);
            SceneManager.LoadScene(LevelNames.MainLevelUI, LoadSceneMode.Additive);
        }

        public void LoadStartMenu()
        {
            SceneManager.LoadScene(LevelNames.MainMenu);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }

    public static class LevelNames
    {
        public const string MainMenu = "MainMenu";
        public const string MainLevel = "MainLevel";
        public const string MainLevelUI = "MainLevelUI";
    }

    public static class GameTime
    {
        private static bool _isPaused;

        public static bool IsPaused
        {
            get => _isPaused;
            set
            {
                _isPaused = value;
                Time.timeScale = _isPaused ? 0f : 1f;
            }
        }
    }
}