using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEngine;

namespace StrategyGame.Controllers
{
    public class MainLevelController : MonoBehaviour
    {
        private static UnitController UnitController => UnitController.Instance;

        private static MainLevelController _instance;

        public static MainLevelController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<MainLevelController>();
                return _instance;
            }
        }

        private LevelState _previousState;
        private LevelState _levelState;

        public delegate void LevelStateDelegate(LevelState newState);

        public LevelStateDelegate OnNewLevelState;

        public LevelState LevelState
        {
            get => _levelState;
            set
            {
                if (_levelState == value) return;
                _previousState = _levelState;
                _levelState = value;
                OnNewLevelState?.Invoke(_levelState);
            }
        }

        public Queue<UnitDisplay> UnitQueue = new();

        public delegate void UnitQueueDelegate();

        public UnitQueueDelegate OnUnitQueueChanged;

        public int CurrentActionCostSum
        {
            get
            {
                var currentActionSum = 0;
                if (UnitQueue.Count > 0)
                {
                    UnitQueue.ToList().ForEach(unit =>
                    {
                        if (unit.ActionsQueue.Count > 0)
                        {
                            unit.ActionsQueue.ToList().ForEach(action => currentActionSum += action.Cost);
                        }
                    });
                }

                return Mathf.Clamp(currentActionSum, 0, GridController.MaxActionCost);
            }
        }

        private void Start()
        {
            OnNewLevelState += newState =>
            {
                if (newState != LevelState.EndLevel) return;
                UIController.Instance.endScreen.gameObject.SetActive(true);
                UIController.Instance.endScreen.SetHeaderText(UnitController.Enemies.Count == 0
                    ? "You have won!"
                    : "The enemy has won!");
            };
            LevelState = LevelState.SetupWorld;
        }

        public void AddUnitToQueue(UnitDisplay unit)
        {
            if (UnitQueue.Contains(unit)) return;
            UnitQueue.Enqueue(unit);
            OnUnitQueueChanged?.Invoke();
        }

        public void RemoveUnitFromQueue(UnitDisplay unit)
        {
            if (!UnitQueue.Contains(unit)) return;
            UnitQueue = new Queue<UnitDisplay>(UnitQueue.ToList().Where(u => u != unit));
            OnUnitQueueChanged?.Invoke();
        }

        public void ExecuteQueue()
        {
            StartCoroutine(Seq_ExecuteQueue());
        }

        private IEnumerator Seq_ExecuteQueue()
        {
            LevelState = LevelState.ExecutingTurn;
            // Execute all actions for all units in queue
            while (UnitQueue.Count > 0)
            {
                yield return UnitQueue.Dequeue().Seq_ExecuteQueue();
                GridController.Instance.State = GridState.Default;
                yield return new WaitForSeconds(0.5f);
            }

            yield return new WaitForSeconds(1f);

            UnitQueue.Clear();
            OnUnitQueueChanged?.Invoke();
            UnitController.DeselectUnit();

            // Check win/lose condition for round
            if (UnitController.Allies.Count == 0 || UnitController.Enemies.Count == 0)
            {
                LevelState = LevelState.EndLevel;
                yield break;
            }

            // Switch turn to other side
            LevelState = _previousState switch
            {
                LevelState.PlayerTurn => LevelState.EnemyTurn,
                LevelState.EnemyTurn => LevelState.PlayerTurn,
                _ => LevelState
            };
        }
    }

    public enum LevelState
    {
        Default = 0,
        Idle = 1,
        SetupWorld = 2,
        SetupUnits = 3,
        PlayerTurn = 4,
        EnemyTurn = 5,
        ExecutingTurn = 6,
        EndLevel = 7
    }
}