using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StrategyGame.Grid;
using StrategyGame.Grid.Pathfinding;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEngine;

namespace StrategyGame.Controllers
{
    public class AIController : MonoBehaviour
    {
        private static MainLevelController MainLevelController => MainLevelController.Instance;
        private static UnitController UnitController => UnitController.Instance;

        private const int AttackTargetMax = 3;

        private IGridVisualizer<GridTile, GridTileDisplay> _gridVisualizer;
        private Pathfinder _pathfinder;

        private void Awake()
        {
            _gridVisualizer = GridController.Instance;
            _pathfinder = GridController.Instance.PathFinder;
            MainLevelController.OnNewLevelState += newState =>
            {
                if (newState != LevelState.EnemyTurn) return;
                StartCoroutine(Seq_MakeTurn());
            };
        }

        private IEnumerator Seq_MakeTurn()
        {
            var units = UnitController.Enemies;
            var targets = UnitController.Allies;
            if (units.Count == 0 || targets.Count == 0) yield break;

            // Attack first if possible
            var attackCost = CreateAttackActionsForUnits(units, targets);

            yield return new WaitForSeconds(1f);

            var remainingCost = GridController.MaxActionCost - attackCost;
            if (remainingCost <= 0) yield break;

            // Move units with remaining action points
            CreateMoveActionsForUnits(units, targets, remainingCost);

            yield return new WaitForSeconds(1f);

            MainLevelController.ExecuteQueue();
        }

        private int CreateAttackActionsForUnits(List<UnitDisplay> units,
            IReadOnlyCollection<UnitDisplay> targets)
        {
            var currentCost = 0;
            foreach (var unit in units)
            {
                // Check for possible attack targets
                var attackTargets = GetUnitsInAttackRange(unit, targets, _pathfinder)
                    .Take(AttackTargetMax).ToList();
                if (attackTargets.Count == 0) continue;

                // Check for every attack target if it's on any tile in the attack area
                var friendlyUnitPositions = units.Select(t => t.CurrentGridPosition);
                foreach (var target in attackTargets)
                {
                    var attackPos = target.unit.CurrentGridPosition;
                    // Get all tiles in attack range, excluding friendly units
                    var area = _gridVisualizer
                        .GetAllGridDisplaysInRange(attackPos, GridController.DefaultAttackRange)
                        .Where(display => !friendlyUnitPositions.Contains(display.Tile.GridPosition)).ToList();

                    var attackTile = area.FirstOrDefault(display => display.Tile.GridPosition == attackPos)?.Tile;
                    if (attackTile == null) continue;

                    UnitController.AddAttackActionToUnit(unit, attackTile,
                        area.Select(display => display.Tile.GridPosition).ToList(), target.cost);
                    currentCost += target.cost;
                }
            }

            return currentCost;
        }

        private void CreateMoveActionsForUnits(IReadOnlyCollection<UnitDisplay> units,
            IReadOnlyCollection<UnitDisplay> targets,
            int remainingCost)
        {
            var unitsByClosestToTarget =
                units.Select(unit => (unit, GetUnitsByDistanceTo(unit, targets).First().distance))
                    .OrderBy(tuple => tuple.distance).ToList();
            var distanceSum = unitsByClosestToTarget.Sum(tuple => tuple.distance);

            foreach (var tuple in unitsByClosestToTarget)
            {
                var targetsByDistance = GetUnitsByDistanceTo(tuple.unit, targets);
                var (dstAverage, dstMin, dstMax) = GetDistancesToUnits(tuple.unit, targets);
                Vector2Int newPos;
                var costForMove = (int) (remainingCost * (tuple.distance / distanceSum));

                if (dstMax - dstAverage <= dstMin + (dstMax - dstMin) * 0.4f)
                {
                    // Most units are rather far away = Move closer
                    var closestTarget = targetsByDistance.FirstOrDefault().unit;
                    if (closestTarget == null) continue;

                    newPos = closestTarget.CurrentGridPosition;
                }
                else
                {
                    // Most units are rather close = Retreat unit
                    var furthestTarget = targetsByDistance.LastOrDefault().unit;
                    if (furthestTarget == null) continue;

                    newPos = new Vector2Int(_gridVisualizer.Grid.GridSize.x - 1,
                        furthestTarget.CurrentGridPosition.y);
                }

                var unitPositions = units.Select(u => u.CurrentGridPosition).ToList();
                var moveCost = Mathf.Clamp(costForMove, 0, GridController.Instance.maxActionCostMove);
                var (path, _) =
                    GridController.Instance.PathFinder.FindPath(tuple.unit.CurrentGridPosition, newPos,
                        maxCost: moveCost,
                        customObstacles: unitPositions);
                path = path.ToList();
                if (!path.Any()) continue;

                var lastPos = path.Last().GridPosition;
                var tile = _gridVisualizer.GetGridTileAt(lastPos);
                if (tile.UnitInPlace != null)
                {
                    path = path.Take(path.Count() - 1);
                }

                UnitController.AddMoveActionToUnit(tuple.unit, path.Select(node => node.GridPosition));
            }
        }

        /// <summary>
        /// Returns a list of units sorted by distance towards a single one
        /// </summary>
        /// <returns>Sorted list of units towards start units</returns>
        private static IEnumerable<(UnitDisplay unit, float distance)> GetUnitsByDistanceTo(UnitDisplay fromUnit,
            IEnumerable<UnitDisplay> toUnits)
        {
            return toUnits.Select(unit =>
                    (unit, distance: Vector2Int.Distance(unit.CurrentGridPosition, fromUnit.CurrentGridPosition)))
                .OrderBy(tuple => tuple.distance);
        }

        /// <summary>
        /// Returns the average, minimum and maximum distance towards a list of units
        /// </summary>
        private static (float average, float min, float max) GetDistancesToUnits(UnitDisplay fromUnit,
            IEnumerable<UnitDisplay> toUnits)
        {
            var distances = toUnits.Select(unit =>
                Vector2Int.Distance(unit.CurrentGridPosition, fromUnit.CurrentGridPosition)).ToList();
            return (distances.Average(), distances.Min(), distances.Max());
        }

        /// <summary>
        /// Returns all units in attack range
        /// </summary>
        /// <returns>List of tuples with units in range and the cost getting towards them</returns>
        private static IEnumerable<(UnitDisplay unit, int cost)> GetUnitsInAttackRange(UnitDisplay fromUnit,
            IEnumerable<UnitDisplay> targets, Pathfinder pathfinder)
        {
            var targetsWithCost = targets.Select(unit => (unit,
                pathfinder.FindPath(fromUnit.CurrentGridPosition, unit.CurrentGridPosition)
                    .cost));
            return targetsWithCost.Where(tuple =>
                tuple.unit != null && tuple.cost >= 0 && tuple.cost <= GridController.Instance.maxActionCostAttack);
        }
    }
}