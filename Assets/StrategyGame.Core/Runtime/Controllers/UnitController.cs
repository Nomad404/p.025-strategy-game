using System.Collections.Generic;
using System.Linq;
using StrategyGame.Grid;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEngine;
using ILogger = StrategyGame.Interfaces.ILogger;

namespace StrategyGame.Controllers
{
    public class UnitController : MonoBehaviour
    {
        private static UnitController _instance;
        private static GridController GridController => GridController.Instance;
        private static MainLevelController MainLevelController => MainLevelController.Instance;

        public static UnitController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<UnitController>();
                return _instance;
            }
        }

        [Header("Spawn settings"), SerializeField]
        private UnitDisplay friendlyUnitPrefab;

        [SerializeField] private UnitDisplay hostileUnitPrefab;
        [Space, SerializeField, Range(1, 4)] private int alliesSpawnSize = 1;
        [SerializeField, Range(2, 8)] private int enemiesSpawnSize = 2;

        public readonly List<UnitDisplay> Allies = new();
        public readonly List<UnitDisplay> Enemies = new();

        private UnitDisplay _currentSelectedUnit;

        public UnitDisplay CurrentSelectedUnit
        {
            get => _currentSelectedUnit;
            private set
            {
                if (_currentSelectedUnit != null) _currentSelectedUnit.IsSelected = false;
                _currentSelectedUnit = value;
                if (_currentSelectedUnit != null) _currentSelectedUnit.IsSelected = true;
                if (value != null)
                {
                    UIController.Instance.EnableModeSwitch();
                }
                else
                {
                    UIController.Instance.DisableModeSwitch();
                }
            }
        }

        private ILogger _logger;

        private void OnValidate()
        {
            enemiesSpawnSize = Mathf.Clamp(enemiesSpawnSize, alliesSpawnSize * 2, 8);
        }

        private void Awake()
        {
            MainLevelController.Instance.OnNewLevelState += OnNewLevelState;
        }

        private void Start()
        {
            _logger = UIController.Instance;
        }

        private void OnNewLevelState(LevelState newState)
        {
            if (newState != LevelState.SetupUnits) return;
            SpawnUnits();
            MainLevelController.LevelState = LevelState.PlayerTurn;
        }

        private void SpawnUnits()
        {
            var gridSize = GridController.gridSize;
            // Spawn allies
            for (var i = 0; i < alliesSpawnSize; i++)
            {
                var gridPos = new Vector2Int(1, gridSize.y / 2 - alliesSpawnSize / 2 + i);
                var tile = GridController.Grid.Tiles[gridPos.x, gridPos.y];
                var worldPos = tile.WorldPosition;
                var unit = Instantiate(friendlyUnitPrefab, new Vector3(worldPos.x, 1, worldPos.z), Quaternion.identity,
                    transform);
                tile.UnitInPlace = unit;
                unit.CurrentGridPosition = gridPos;
                unit.name = $"Ally #{i}";
                unit.Faction = UnitDisplay.UnitFaction.Friendly;
                unit.TurnTowards(new Vector3(worldPos.x + 1, unit.transform.position.y, worldPos.z));
                Allies.Add(unit);
            }

            // Spawn enemies
            for (var i = 0; i < enemiesSpawnSize; i++)
            {
                var gridPos = new Vector2Int(GridController.gridSize.x - 2, gridSize.y / 2 - enemiesSpawnSize / 2 + i);
                var tile = GridController.Grid.Tiles[gridPos.x, gridPos.y];
                var worldPos = tile.WorldPosition;
                var unit = Instantiate(hostileUnitPrefab, new Vector3(worldPos.x, 1, worldPos.z), Quaternion.identity,
                    transform);
                tile.UnitInPlace = unit;
                unit.CurrentGridPosition = gridPos;
                unit.name = $"Enemy #{i}";
                unit.Faction = UnitDisplay.UnitFaction.Hostile;
                unit.TurnTowards(new Vector3(worldPos.x - 1, unit.transform.position.y, worldPos.z));
                Enemies.Add(unit);
            }
        }

        public void SelectUnit(UnitDisplay unit)
        {
            CurrentSelectedUnit = unit;
            GridController.State = GridState.ShowMovePath;
            UIController.Instance.gridModeSwitchButton.UpdateByGridState(GridController.State);
        }

        public void DeselectUnit()
        {
            CurrentSelectedUnit = null;
            GridController.Reset();
        }

        public void AddMoveActionToUnit(UnitDisplay unit, IEnumerable<Vector2Int> path)
        {
            if (unit == null) return;
            // Create copy for consistency
            var currentPath = new List<Vector2Int>(path);
            if (currentPath.Count == 0)
            {
                _logger.AddLogMessage($"{unit.name}: Move path is empty", Color.red);
                return;
            }

            // Remove all not walkable nodes starting at the last one
            while (currentPath.Count > 0 && !GridController.GetGridTileAt(currentPath.Last()).IsWalkable())
            {
                currentPath.Remove(currentPath.Last());
            }

            if (currentPath.Count <= 0)
            {
                _logger.AddLogMessage($"{unit.name}: Path not walkable", Color.red);
                return;
            }

            // Move action costs too many points to execute
            if (MainLevelController.CurrentActionCostSum + GridController.CurrentPathCost >
                GridController.MaxActionCost)
            {
                _logger.AddLogMessage($"{unit.name}: Action exceeds max cost", Color.red);
                return;
            }

            unit.AddActionToQueue(new UnitActionMove(unit, currentPath, GridController.CurrentPathCost,
                GridController.Grid.Tiles, _logger));
            _logger.AddLogMessage($"{unit.name}: Added move action");
            MainLevelController.AddUnitToQueue(unit);
        }

        public void AddAttackActionToUnit(UnitDisplay unit, GridTile tileToAttack, List<Vector2Int> area,
            int actionCost)
        {
            if (tileToAttack.IsObstacle) return;
            if (unit == null) return;
            if (area.Count == 0)
            {
                _logger.AddLogMessage($"{unit.name}: Attack area is empty", Color.red);
                return;
            }

            if (!area.Contains(tileToAttack.GridPosition))
            {
                _logger.AddLogMessage($"{unit.name}: Target not in range", Color.red);
                return;
            }

            if (tileToAttack.UnitInPlace == null)
            {
                _logger.AddLogMessage($"{unit.name}: No unit on attack tile", Color.red);
                return;
            }

            if (tileToAttack.UnitInPlace != null && tileToAttack.UnitInPlace.Faction == unit.Faction)
            {
                _logger.AddLogMessage($"{unit.name}: You can't attack friendly units", Color.red);
                return;
            }

            if (MainLevelController.CurrentActionCostSum + actionCost > GridController.MaxActionCost)
            {
                _logger.AddLogMessage($"{unit.name}: Action exceeds max cost", Color.red);
                return;
            }

            unit.AddActionToQueue(new UnitActionAttack(unit, tileToAttack, unit.LatestActionPoint, area, actionCost,
                _logger));
            _logger.AddLogMessage($"{unit.name}: Added attack action");
            MainLevelController.AddUnitToQueue(unit);
        }
    }
}