using System;
using System.Collections.Generic;
using System.Linq;
using StrategyGame.Grid;
using StrategyGame.Grid.Pathfinding;
using StrategyGame.Interfaces;
using StrategyGame.Units;
using UnityEditor;
using UnityEngine;

namespace StrategyGame.Controllers
{
    public class GridController : MonoBehaviour, IGridVisualizer<GridTile, GridTileDisplay>
    {
        private static GridController _instance;

        public static GridController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<GridController>();
                return _instance;
            }
        }

        [Header("Grid settings")] public Vector2Int gridSize;
        [SerializeField] private Vector3 tileOffset;
        [SerializeField] private GridTileDisplay tileDisplayPrefab;
        [SerializeField] private List<Vector2Int> obstaclePositions = new();

        [Header("Actions"), Range(10, 100)] public int maxActionCostMove = 50;
        [Range(10, 50)] public int maxActionCostAttack = 30;
        public const int MaxActionCost = 150;
        private int _currentPathCost;

        public const float DefaultAttackRange = 5f;

        [Space] public bool autoUpdate;

        public GridState State { get; set; }

        public Grid<GridTile> Grid { get; private set; }
        public GridTileDisplay[,] GridDisplays { get; private set; }
        public readonly Pathfinder PathFinder = new();
        public GridTile CurrentPointedTile { get; set; }
        public List<Vector2Int> CurrentTileSelection { get; private set; } = new();

        public int CurrentPathCost
        {
            get => _currentPathCost;
            private set => _currentPathCost = Mathf.Clamp(value, 0, MaxActionCost);
        }

        private void Awake()
        {
            MainLevelController.Instance.OnNewLevelState += OnNewLevelState;
        }

        private void OnNewLevelState(LevelState newState)
        {
            if (newState != LevelState.SetupWorld) return;
            ClearGrid();
            GenerateFullGrid();
            MainLevelController.Instance.LevelState = LevelState.SetupUnits;
        }

        public void GenerateFullGrid()
        {
            ClearGrid();
            SetupGrid();
            SetupGridTileDisplays();
        }

        public GridTile GetGridTileAt(Vector2Int pos)
        {
            return Grid.Tiles[pos.x, pos.y];
        }

        public static void ClearGrid()
        {
            FindObjectsOfType<GridTileDisplay>().ToList().ForEach(tile =>
            {
                // Necessary for running within the editor
#if UNITY_EDITOR
                if (!EditorApplication.isPlaying)
                {
                    DestroyImmediate(tile.gameObject);
                }
                else
                {
                    Destroy(tile.gameObject);
                }
#else
                Destroy(tile.gameObject);
#endif
            });
        }

        private void SetupGrid()
        {
            var tileSizeX = tileDisplayPrefab.DisplaySize.x;
            var tileSizeY = tileDisplayPrefab.DisplaySize.z;
            var gridHalfWidth = gridSize.x / 2;
            var gridHalfHeight = gridSize.y / 2;
            Grid = new Grid<GridTile>(gridSize, gridPos =>
            {
                var worldPos = new Vector3((gridPos.x - gridHalfWidth) * tileSizeX * (1 + tileOffset.x), tileOffset.y,
                    (gridPos.y - gridHalfHeight) * tileSizeY * (1 + tileOffset.z));
                return new GridTile(gridPos, worldPos, this, obstaclePositions.Contains(gridPos));
            });
            PathFinder.GenerateGrid(gridSize, obstaclePositions);
        }

        private void SetupGridTileDisplays()
        {
            GridDisplays = new GridTileDisplay[gridSize.x, gridSize.y];
            for (var y = 0; y < Grid.Tiles.GetLength(1); y++)
            {
                for (var x = 0; x < Grid.Tiles.GetLength(0); x++)
                {
                    var tile = Grid.Tiles[x, y];
                    var display = Instantiate(tileDisplayPrefab, tile.WorldPosition, Quaternion.identity, transform);
                    display.GridVisualizer = this;

                    var index = gridSize.x * tile.GridPosition.y + tile.GridPosition.x;
                    display.name = $"Tile #{index}";
                    display.Tile = tile;
                    var isEven = x % 2 == 0 && y % 2 != 0 || x % 2 != 0 && y % 2 == 0;
                    display.IsEven = isEven;
                    GridDisplays[x, y] = display;
                }
            }
        }

        private void Update()
        {
            var unit = UnitController.Instance.CurrentSelectedUnit;
            if (unit != null)
            {
                switch (State)
                {
                    case GridState.Default:
                        break;
                    case GridState.ShowMovePath:
                        UpdatePotentialMovePath(unit);
                        break;
                    case GridState.ShowAttackRange:
                        UpdatePotentialAttackArea(unit);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Updates the path from a unit towards the currently pointed at tile
        /// </summary>
        private void UpdatePotentialMovePath(UnitDisplay unit)
        {
            if (CurrentPointedTile is {IsObstacle: true}) return;
            var startPos = unit.CurrentGridPosition;

            // Override the start position of the next path if unit has actions
            if (unit.ActionsQueue.Count > 0)
            {
                var latestAction = unit.ActionsQueue.Last();
                startPos = latestAction switch
                {
                    UnitActionMove => latestAction.TargetPoint,
                    UnitActionAttack => latestAction.SourcePoint,
                    _ => startPos
                };
            }

            // Find shortest path to the pointed position if the position is valid
            var (pathNodes, cost) = PathFinder.FindPath(startPos,
                (CurrentPointedTile ?? Grid.GetObjectAt(startPos)).GridPosition, maxActionCostMove);
            var path = pathNodes.Select(node => node.GridPosition).ToList();

            if (path.Count == 0 || path == CurrentTileSelection) return;
            CurrentTileSelection = path;
            CurrentPathCost = cost;
        }

        /// <summary>
        /// Updates the attack area around a unit
        /// </summary>
        private void UpdatePotentialAttackArea(UnitDisplay unit)
        {
            var startPos = unit.CurrentGridPosition;
            // Override the center of the attack area, if other actions are in the queue of the unit
            if (unit.ActionsQueue.Count > 0)
            {
                var latestAction = unit.ActionsQueue.Last();
                startPos = latestAction switch
                {
                    UnitActionMove => latestAction.TargetPoint,
                    UnitActionAttack => latestAction.SourcePoint,
                    _ => startPos
                };
            }

            var area = GetAllGridDisplaysInRange(startPos, DefaultAttackRange).Where(display =>
            {
                // Only allow tiles that can be reached with a maximum move cost, to exclude tiles on the other side of obstacles
                var (path, _) = PathFinder.FindPath(startPos, display.Tile.GridPosition,
                    ignoreObstacles: true, maxCost: maxActionCostAttack);
                return !display.Tile.IsObstacle && path.Count(node => node.IsObstacle) == 0;
            }).Select(display => display.Tile.GridPosition).ToList();
            if (area.Count == 0 || area == CurrentTileSelection) return;

            var (attackPath, cost) = PathFinder.FindPath(startPos,
                (CurrentPointedTile ?? Grid.GetObjectAt(startPos)).GridPosition, maxActionCostAttack);
            CurrentTileSelection = area;
            if (attackPath == null || !attackPath.Any()) return;

            CurrentPathCost = cost;
        }

        public void Reset()
        {
            CurrentTileSelection = new List<Vector2Int>();
            State = GridState.Default;
        }

        private IEnumerable<GridTileDisplay> GetAllDisplaysFlat()
        {
            var result = new List<GridTileDisplay>();
            for (var x = 0; x < GridDisplays.GetLength(0); x++)
            {
                for (var y = 0; y < GridDisplays.GetLength(1); y++)
                {
                    result.Add(GridDisplays[x, y]);
                }
            }

            return result;
        }

        public IEnumerable<GridTileDisplay> GetAllGridDisplaysInRange(Vector2Int gridPos, float distance)
        {
            var centerTile = Grid.Tiles[gridPos.x, gridPos.y];
            return GetAllDisplaysFlat().Where(display =>
                Vector3.Distance(centerTile.WorldPosition, display.Tile.WorldPosition) <= distance).ToList();
        }
    }
}