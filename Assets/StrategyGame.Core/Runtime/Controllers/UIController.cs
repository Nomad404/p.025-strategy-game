using System.Collections.Generic;
using System.Diagnostics;
using StrategyGame.UI;
using UnityEngine;
using ILogger = StrategyGame.Interfaces.ILogger;

namespace StrategyGame.Controllers
{
    public class UIController : MonoBehaviour, ILogger
    {
        private static UIController _instance;

        public static UIController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<UIController>();
                return _instance;
            }
        }

        [SerializeField] private GameObject pauseMenu;
        [SerializeField] private GameObject messageLogContainer;
        [SerializeField] private UISubmitButton submitButton;
        public UIGridModeSwitchButton gridModeSwitchButton;
        public UITurnIndicator turnIndicator;
        public UIEndScreen endScreen;

        [Header("Prefabs"), SerializeField] private UILogMessage logMessagePrefab;
        private readonly List<UILogMessage> _logMessages = new();
        private const float LogClearTimeout = 3000f;
        private readonly Stopwatch _logTimer = new();

        private void Update()
        {
            CheckLogTimer();
        }

        public void EnableModeSwitch()
        {
            gridModeSwitchButton.button.interactable = true;
        }

        public void DisableModeSwitch()
        {
            gridModeSwitchButton.button.interactable = false;
        }

        private void CheckLogTimer()
        {
            if (!_logTimer.IsRunning || !(_logTimer.ElapsedMilliseconds > LogClearTimeout)) return;
            if (_logMessages.Count > 0)
            {
                var message = _logMessages[0];
                _logMessages.Remove(message);
                Destroy(message.gameObject);
                if (_logMessages.Count > 0)
                {
                    _logTimer.Restart();
                    return;
                }
            }

            _logTimer.Stop();
        }

        public void AddLogMessage(string text)
        {
            AddLogMessage(text, Color.white);
        }

        public void AddLogMessage(string text, Color textColor)
        {
            var message = Instantiate(logMessagePrefab, messageLogContainer.transform);
            message.SetText(text);
            message.SetTextColor(textColor);
            _logMessages.Add(message);
            _logTimer.Restart();
        }

        public void RestartMainLevel()
        {
            GameController.Instance.LoadMainLevel();
        }

        public void ReturnToMainMenu()
        {
            GameController.Instance.LoadStartMenu();
        }

        public void QuitGame()
        {
            GameController.Instance.QuitGame();
        }
    }
}