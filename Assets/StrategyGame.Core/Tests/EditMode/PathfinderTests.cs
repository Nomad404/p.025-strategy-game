using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StrategyGame.Grid.Pathfinding;
using UnityEngine;

namespace Tests.EditMode
{
    public class PathfinderTests
    {
        [Test]
        public void TestPathfinder_FindPath()
        {
            var gridSize = new Vector2Int(10, 10);
            var obstacles = new List<Vector2Int>
            {
                new(1, 1),
                new(6, 6)
            };
            var pathFinder = new Pathfinder();
            pathFinder.GenerateGrid(gridSize, obstacles);
            Assert.AreEqual(pathFinder.Grid.Tiles.GetLength(0), gridSize.x);
            Assert.AreEqual(pathFinder.Grid.Tiles.GetLength(1), gridSize.y);

            // Make a diagonal path, ignoring all obstacles
            var (path, cost) = pathFinder.FindPath(new Vector2Int(0, 0), new Vector2Int(gridSize.x - 1, gridSize.y - 1),
                ignoreObstacles: true);
            Assert.AreEqual(path.Count(), 10);
            Assert.AreEqual(cost, 126);

            // Same path, but now including obstacles
            (path, cost) = pathFinder.FindPath(new Vector2Int(0, 0), new Vector2Int(gridSize.x - 1, gridSize.y - 1));
            Assert.AreEqual(path.Count(), 11);
            Assert.AreEqual(cost, 132);
        }
    }
}