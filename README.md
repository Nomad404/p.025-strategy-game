# Strategy Game

This prototype was made for the goal to create a small, turn-based game with moving units.

## Rules

The player fights against the AI. The goal of each round is to eliminate all opposing units.

The player can:
- Select units
- Select tiles while unit is selected
- Switch mode between moving and attacking with the currently selected unit
- Add multiple actions to a unit (if it doesn't exceed the max cost) 

Both the AI and the player have a finite amount of cost points for possible actions on each turn.

![Main Game](Images/MainGame.png)

## Resources

- [A* pathfinding by Code Monkey](https://www.youtube.com/watch?v=alU04hvz6L4&t=1123s)
- [A* pathfinding by Sebastian Lague](https://www.youtube.com/watch?v=-L-WgKMFuhE&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW)
- [Grid generation by Code Monkey](https://www.youtube.com/watch?v=waEsGu--9P8)
- [Grid generation by Tarodev](https://www.youtube.com/watch?v=kkAjpQAM-jE)